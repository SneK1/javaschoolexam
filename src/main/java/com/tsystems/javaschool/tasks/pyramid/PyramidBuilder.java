package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int n = inputNumbers.size();
        int x = (int)Math.floor(Math.sqrt(n * 2));
        if (n != x * (x + 1) / 2) throw new CannotBuildPyramidException(); // 1+2+...+n=x(x+1)/2
        int[][] resultPyramid = new int[x][x * 2 - 1];

        Collections.sort(inputNumbers);
        int step = 0; // for inputNumbers

        for(int row = 0; row < resultPyramid.length; row++){
            x--;
            for(int col = x; col < resultPyramid[0].length - x; col+=2){
                resultPyramid[row][col] = inputNumbers.get(step++);
            }
        }
        return resultPyramid;
    }


}
