package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        class PostfixExpression {
            private final String operators = "+-*/";
            private final String delimiters = "()" + operators;

            private boolean isOperator(String token) {
                if (token.length() != 1) return false;
                else return operators.contains(token);
            }

            private boolean isDelimiter(String token) {
                if (token.length() != 1) return false;
                else return delimiters.contains(token);
            }


            private int priority(String token) {
                if (token.equals("(")) return 1;
                if (token.equals("+") || token.equals("-")) return 2;
                if (token.equals("*") || token.equals("/")) return 3;
                return 4;
            }

            private List<String> parse(String infix) {
                List<String> postfix = new ArrayList<>();
                Deque<String> stack = new ArrayDeque<>();
                StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
                String curr = "";
                String prev = "";
                while (tokenizer.hasMoreTokens()) {
                    curr = tokenizer.nextToken();
                    if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                        return null;
                    }
                    //if (curr.equals(" ")) continue;
                    if (isDelimiter(curr)) {
                        switch (curr) {
                            case "(":
                                stack.push(curr);
                                break;
                            case ")":
                                if (stack.peek() == null) return null;
                                while (!stack.peek().equals("(")) {
                                    postfix.add(stack.pop());
                                    if (stack.isEmpty()) { // miss "(" for ")"
                                        return null;
                                    }
                                }
                                stack.pop();
                                break;
                            default:  // operators
                                if (isOperator(prev)) return null; // ++, -- ...
                                while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                                    postfix.add(stack.pop());
                                }

                                stack.push(curr);
                                break;
                        }
                    } else { // number
                        postfix.add(curr);
                    }
                    prev = curr;
                }

                while (!stack.isEmpty()) {
                    if (isOperator(stack.peek())) postfix.add(stack.pop());
                    else {
                        return null;
                    }
                }
                return postfix;
            }

            private Double calc(List<String> postfix) {
                Deque<Double> stack = new ArrayDeque<>();
                for (String x : postfix) {
                    switch (x) {
                        case "+":
                            stack.push(stack.pop() + stack.pop());
                            break;
                        case "-":
                            Double b = stack.pop(), a = stack.pop();
                            stack.push(a - b);
                            break;
                        case "*":
                            stack.push(stack.pop() * stack.pop());
                            break;
                        case "/":
                            Double c = stack.pop(), d = stack.pop();
                            if (c == 0) return null;
                            stack.push(d / c);
                            break;
                        default:
                            try {
                                stack.push(Double.parseDouble(x));
                            } catch (NullPointerException | NumberFormatException e) {
                                return null;
                            }
                            break;
                    }
                }
                return stack.pop();
            }

        }

        if (statement == null || statement.length() == 0) return null;
        PostfixExpression postfixExpression = new PostfixExpression();
        List<String> postfix = postfixExpression.parse(statement);
        if (postfix == null) return null;
        else {
            Double result = postfixExpression.calc(postfix);
            if (result == null) return null;
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat df = (DecimalFormat) nf;
            df.applyPattern("#.####");
            return df.format(result);
        }
    }

}
